﻿using System;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PMS_lab8
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Random random = new Random();
        private DispatcherTimer timer = new DispatcherTimer();
        private double canvasWidth;
        private double canvasHeight;

        public MainWindow()
        {
            InitializeComponent();

            /*var rotateAnimation = new DoubleAnimation()
            {
                From = 0,
                To = 360,
                Duration = TimeSpan.FromSeconds(10),
                RepeatBehavior = RepeatBehavior.Forever,
            };
            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);*/

            timer.Interval = TimeSpan.FromMilliseconds(30); // Update every 30 milliseconds
            timer.Tick += Timer_Tick;
            timer.Start();

            // Store the canvas dimensions
            canvasWidth = canvas.ActualWidth;
            canvasHeight = canvas.ActualHeight;

            // Listen for changes in the canvas size
            canvas.SizeChanged += Canvas_SizeChanged;
        }

        private void Canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // Update canvas dimensions when size changes
            canvasWidth = canvas.ActualWidth;
            canvasHeight = canvas.ActualHeight;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (canvas.Children.Count < random.Next(5, 25))
            {
                //timer.Stop();
                // Create a new ball
                CreateBall();
            }

            // Update the position of each ball
            /*foreach (UIElement ball in canvas.Children)
            {
                double left = Canvas.GetLeft(ball);
                double top = Canvas.GetTop(ball);

                // Calculate new position
                double dx = random.Next(-5, 6); // Random horizontal movement between -5 and 5
                double dy = random.Next(-5, 6); // Random vertical movement between -5 and 5
                left += dx;
                top += dy;

                // Ensure the ball stays within the canvas boundaries
                left = Math.Max(0, Math.Min(left, canvasWidth - ball.RenderSize.Width));
                top = Math.Max(0, Math.Min(top, canvasHeight - ball.RenderSize.Height));

                // Update ball position
                Canvas.SetLeft(ball, left);
                Canvas.SetTop(ball, top);
            }*/
        }

        private void CreateBall()
        {
            // Create a new Ellipse (ball)
            Ellipse ball = new Ellipse();
            ball.Width = 10;
            ball.Height = 10;

            // Generate a random color
            Color randomColor = Color.FromRgb((byte)random.Next(256), (byte)random.Next(256), (byte)random.Next(256));
            SolidColorBrush brush = new SolidColorBrush(randomColor);
            ball.Fill = brush;

            // Generate random position for the ball
            double left = random.NextDouble() * (canvas.ActualWidth - ball.Width);
            double top = random.NextDouble() * (canvas.ActualHeight - ball.Height);
            Canvas.SetLeft(ball, left);
            Canvas.SetTop(ball, top);

            //
            ball.RenderTransformOrigin = new Point(0.5, 0.5);
            ball.RenderTransform = new TransformGroup()
            {
                Children =
                {
                    new TranslateTransform()
                    {
                        X = 0,
                        Y = -100,
                    },
                    new RotateTransform(),
                },  
            };

            var rotateAnimation = new DoubleAnimation()
            {
                From = 0,
                To = 360,
                Duration = TimeSpan.FromSeconds(10),
                RepeatBehavior = RepeatBehavior.Forever,
            };
            RotateTransform rotateTransform = ((TransformGroup)ball.RenderTransform).Children.OfType<RotateTransform>().FirstOrDefault();
            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);

            //
            //ball.Loaded += startRotating;

            // Add the ball to the canvas
            canvas.Children.Add(ball);



            //
            /*var rotateAnimation = new DoubleAnimation()
            {
                From = 0,
                To = 360,
                Duration = TimeSpan.FromSeconds(10),
                RepeatBehavior = RepeatBehavior.Forever,
            };
            ball.RenderTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);*/





            /*DoubleAnimation leftAnimation = new DoubleAnimation
            {
                From = 200,
                To = 0,
                Duration = TimeSpan.FromSeconds(5),
                RepeatBehavior = RepeatBehavior.Forever
            };

            DoubleAnimation topAnimation = new DoubleAnimation
            {
                From = 0,
                To = 200,
                Duration = TimeSpan.FromSeconds(5),
                RepeatBehavior = RepeatBehavior.Forever
            };

            // Set the target properties for the animations
            Storyboard.SetTarget(leftAnimation, ball);
            Storyboard.SetTargetProperty(leftAnimation, new PropertyPath(Canvas.LeftProperty));

            Storyboard.SetTarget(topAnimation, ball);
            Storyboard.SetTargetProperty(topAnimation, new PropertyPath(Canvas.TopProperty));

            // Create the storyboard and add animations
            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(leftAnimation);
            storyboard.Children.Add(topAnimation);

            // Start the animation
            storyboard.Begin();*/
        }

        /*private void startRotating(object sender, RoutedEventArgs e)
        {
            var rotateAnimation = new DoubleAnimation()
            {
                From = 0,
                To = 360,
                Duration = TimeSpan.FromSeconds(10),
                RepeatBehavior = RepeatBehavior.Forever,
            };
            RotateTransform rotateTransform = ((TransformGroup)((Ellipse)sender).RenderTransform).Children.OfType<RotateTransform>().FirstOrDefault();            
            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);

            //((Ellipse)sender).RenderTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);
        }*/

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            foreach(var child in canvas.Children)
            {
                if(child is Ellipse ball)
                {
                    Color randomColor = Color.FromRgb((byte)random.Next(256), (byte)random.Next(256), (byte)random.Next(256));
                    SolidColorBrush brush = new SolidColorBrush(randomColor);
                    ball.Fill = brush;
                }
            }
        }
        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var child in canvas.Children)
            {
                if (child is Ellipse ball)
                {
                    ball.Width = random.Next(10, 25);
                    ball.Height = random.Next(10, 25);
                }
            }
        }

        private void Space_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                foreach(var child in canvas.Children)
                {
                    if(child is Ellipse ball)
                    {                        
                        if(ball.RenderTransform != null)
                        {
                            ball.RenderTransform = null;
                        }
                        else
                        {                            
                            ball.RenderTransform = new TransformGroup()
                            {
                                Children =
                                {
                                    new TranslateTransform()
                                    {
                                        X = 0,
                                        Y = -100,
                                    },
                                    new RotateTransform(),
                                },
                            };

                            var rotateAnimation = new DoubleAnimation()
                            {
                                From = 0,
                                To = 360,
                                Duration = TimeSpan.FromSeconds(10),
                                RepeatBehavior = RepeatBehavior.Forever,
                            };
                            RotateTransform rotateTransform = ((TransformGroup)ball.RenderTransform).Children.OfType<RotateTransform>().FirstOrDefault();
                            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);
                        }
                    }
                }
            }
        }
    }
}